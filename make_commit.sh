#!/bin/bash

set -e

tmp_log=.make_commit.log
git status > $tmp_log
if grep -q -E "modified:[ ]+bib/" $tmp_log
then
    ## first pull from remote
    git stash
    git pull gitlab main
    git stash pop
    ## then make commit
    git add -u bib/
    git commit -m "update bib entries"
    git push gitlab main
    ## only sync library on the work machine
    if [ "$HOSTNAME" = "GMC74XVF9K" ]
    then
        bash .sync_library.sh
    else
        sleep 1
    fi
    printf "\n"
    Rscript check_completeness.R
else
    printf "No bib file was updated.\n"
fi
rm $tmp_log
