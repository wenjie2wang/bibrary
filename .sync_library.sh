#!/bin/bash

set -e

## pull from hub
rsync -uavzhPe ssh tars-sh:~/bibrary/articles/ articles/
rsync -uavzhPe ssh tars-sh:~/bibrary/books/ books/
rsync -uavzhPe ssh tars-sh:~/bibrary/others/ others/

## push to hub
rsync -uavzhPe ssh articles/ tars-sh:~/bibrary/articles/
rsync -uavzhPe ssh books/ tars-sh:~/bibrary/books/
rsync -uavzhPe ssh others/ tars-sh:~/bibrary/others/
