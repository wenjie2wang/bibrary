function(data, type, row, meta) {
    // NOTE: it is a template script
    // where `URL_COL` should/would be replaced with the column number of URL
    return "<span><a href=" + row[URL_COL] +
        " target=\"_blank\">" + data + "</a></span>";
}
