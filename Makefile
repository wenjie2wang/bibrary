BIB_DIR := bib
BIB_FILES := $(BIB_DIR)/articles.bib $(BIB_DIR)/books.bib $(BIB_DIR)/others.bib
BIB_UPDATE := $(BIB_DIR)/.last_updated

define render_rmd
	Rscript -e "rmarkdown::render('$(1)')"
endef

.PHONY: all
all: index.html

.PHONY: update
update: $(BIB_UPDATE)

.PHONY: check
check: check_completeness.R
	@Rscript $<

.PHONY: push
push:
	@bash make_commit.sh

.PHONY: sync
sync:
	@bash .sync_library.sh

$(BIB_UPDATE): $(BIB_FILES)
	@printf "needs updating: $(?F)\n\n"
	@Rscript update_bib.R $?
	@touch $@

index.html: index.Rmd $(BIB_FILES)
	@$(call render_rmd,$<)
