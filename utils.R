library(parallel)
library(formatBibtex)

## directory to save pdf
TO_ADD_DIR <- "pending"

## help functions
get_bib_content <- function(x) {
    out <- unclass(x[[names(x)]])[[1]]
    attr(out, "key") <- NULL
    out
}

get_bib_type <- function(x) {
    tolower(attr(get_bib_content(x), "bibtype"))
}

get_hash <- function(x) {
    substr(digest::digest(get_bib_content(x)), 1, 7)
}

get_bib_key <- function(x, add_hash = FALSE) {
    key0 <- names(x)
    x_title <- x[[key0]]$title
    ## reformat title
    new_title <- tolower(gsub(
        "[ ]+|'|-|/", " ",
        gsub("\\\\|'|,|`|[:]|\\n|\\{|\\}|\"|\\+", "", x_title)
    ))
    x_words <- unlist(strsplit(new_title, split = " "))
    idx <- ! x_words %in% format_options$get("lowercase_words")
    first_word <- x_words[idx][1]
    first_author_last_name <- x[[key0]]$author[[1]]$family
    ## if no author, try editor
    if (length(first_author_last_name) == 0) {
        first_author_last_name <- x[[key0]]$editor[[1]]$family
    }
    first_author_last_name <- unlist(strsplit(tolower(
        gsub("[-+]", " ",
             gsub("\\\\|'", "", first_author_last_name))
    ), split = " "))
    idx <- ! first_author_last_name %in% c("de", "van", "der", "den")
    first_author_last_name <- first_author_last_name[idx][1]
    first_author_last_name <- iconv(
        first_author_last_name, from = "UTF-8", to = "ASCII//TRANSLIT"
    )
    ## only keey letters
    first_author_last_name <- gsub("[^(a-zA-Z)]", "", first_author_last_name)
    x_year <- x[[key0]]$year
    if (add_hash) {
        hash_id <- get_hash(x)
        paste0(first_author_last_name, x_year, first_word, "-", hash_id)
    } else {
        paste0(first_author_last_name, x_year, first_word)
    }
}

## read multiple bib files
read_bib_files <- function(x) {
    mclapply(x, bibtex::read.bib,
             mc.cores = min(length(x), detectCores())) |>
        do.call(c, args = _)
}

## main function
update_bib_file <- function(bib_file, rename = NULL) {
    bibList <- bibtex::read.bib(bib_file)
    new_keys <- old_keys <- names(bibList)
    ## not update the key if it current key ends with "-[:alnum:]{7}"
    needs_update <- ! grepl("-[0-9a-z]{7}$", old_keys)
    has_update <- any(needs_update)
    if (has_update) {
        tmp_keys <- sapply(bibList[needs_update], get_bib_key, add_hash = TRUE)
        new_keys[needs_update] <- tmp_keys
    }
    if (anyDuplicated(new_keys)) {
        stop("Found duplicated key(s):\n",
             sprintf("%s\n", new_keys[duplicated(new_keys)]))
    }
    tmp <- grepl("\\+|\\\\|/", new_keys)
    if (any(tmp)) {
        stop("Found problematic key(s):\n",
             sprintf("%s\n", new_keys[tmp]))
    }
    if (has_update) {
        message(sprintf("%s --> %s\n",
                        old_keys[needs_update],
                        new_keys[needs_update]))
        ## update the keys
        for (k in seq_along(which(needs_update))) {
            bibList[needs_update]$key[k] <- new_keys[needs_update][k]
        }
        ## bibtex::write.bib(bibList, file = bib_file)
        message("Updated bib key(s).\n")
        ## try to add pdf files from ./to-add/ to the corresponding folders
        for (k in seq_along(old_keys[needs_update])) {
            old_pdf <- {
                old_k <- bibList[needs_update][k]
                if (! is.null(old_k$file)) {
                    ## if the bibtex has the file entry, from i-librarian
                    old_pdf_id <- gsub(":PDF$", "", basename(old_k$file))
                    ## clear file and keywords
                    bibList[needs_update][k]$keywords <- NULL
                    bibList[needs_update][k]$file <- NULL
                    ## remove {} in title
                    bibList[needs_update][k]$title <-
                        gsub("\\{|\\}", "", old_k$title)
                } else {
                    ## otherwise, match the bib key
                    old_pdf_id <- paste0(old_keys[needs_update][k], ".pdf")
                }
                file.path(TO_ADD_DIR, old_pdf_id)
            }
            new_pdf <- paste0(new_keys[needs_update][k], ".pdf")
            bt_k <- get_bib_type(bibList[needs_update][k])
            target_dir <- switch(bt_k,
                                 "article" = "articles",
                                 "book" = "books",
                                 "others")
            new_pdf <- file.path(target_dir, new_pdf)
            ## main
            if (file.exists(old_pdf)) {
                file.rename(from = old_pdf, to = new_pdf)
                message("renamed ", old_pdf, " --> ", new_pdf)
            }
            ## supplementary
            old_pdf <- gsub("\\.pdf$", "-supplementary.pdf", old_pdf)
            if (file.exists(old_pdf)) {
                new_pdf <- gsub("\\.pdf$", "-supplementary.pdf", new_pdf)
                file.rename(from = old_pdf, to = new_pdf)
                message("renamed ", old_pdf, " --> ", new_pdf)
            }
        }
    } else {
        message("No bib key needs updating for ", basename(bib_file), ".")
    }

    ## format bibtex entries
    extra_protection_words <- c(
        "BERT", "BMC", "Bernoulli", "bioRxiv",
        "CNN", "CoRR",
        "DNA",
        "Fourier",
        "IFAC", "IJDWM",
        "JDS", "JAMA",
        "Luce",
        "Markovian", "MLE",
        "NRL", "NURBS",
        "PAC", "PLOS", "Poisson", "Plackett",
        "RNA", "ROC",
        "SMEM", "SORT", "SiRNA", "siRNA", "siRNAs", "StatsRef",
        "TOG", "TOMACS", "TOMS", "Tweedie",
        "USENIX"
    )
    format_options$append("protected_words", extra_protection_words)
    bib_list <- format_bibtex_entry(
        entry = bibList,
        fields = c("title", "author", "journal", "pages")
    )
    if (! is.null(rename)) {
        bib_file <- sprintf("%s.bib", rename)
    }
    writeLines(bib_list, con = bib_file)
    status <- system2(
        "emacs",
        args = sprintf(paste(
            "--batch -Q %s",
            "--eval '(setq make-backup-files nil)'",
            "--eval '(setq-default fill-column 80)'",
            "--eval '(setq-default indent-tabs-mode nil)'",
            "-f bibtex-reformat",
            "-f bibtex-sort-buffer",
            "-f save-buffer"
        ), bib_file),
        stdout = TRUE,
        stderr = TRUE
    )
    invisible()
}
